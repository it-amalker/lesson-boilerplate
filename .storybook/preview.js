import React from 'react';
import { addDecorator } from '@storybook/react';
import '../src/index.css';
import './preview.css';

addDecorator(storyFn => <div id="lesson-root">{storyFn()}</div>);
