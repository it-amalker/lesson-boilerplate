module.exports = {
  stories: ['../src/**/*.stories.[jt]s*'],
  addons: [
    '@storybook/addon-actions/register',
    '@storybook/addon-links',
    '@storybook/addon-knobs/register',
  ],
  webpackFinal: async config => {
    config.resolve.modules.push(process.cwd() + '/src');
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve('ts-loader'),
          options: {
            compilerOptions: {
              noEmit: false,
            },
          },
        },
      ],
    });
    config.resolve.extensions.push('.ts', '.tsx');
    return config;
  },
};
