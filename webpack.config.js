const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const DefinePlugin = require('webpack').DefinePlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const config = require('./public/lesson.config.json');

const prodPlugins = [
  new CompressionPlugin({
    filename: '[path].br[query]',
    algorithm: 'brotliCompress',
    compressionOptions: {
      level: 11,
    },
    deleteOriginalAssets: false,
    test: [/\.(js|css|html|svg)$/, /\.gl(tf|b)$/],
  }),
  new ZipPlugin({
    path: '../zip',
    filename: 'packed_bundle',
  }),
];

module.exports = (env = {}) => ({
  entry: env.production ? './src/main.tsx' : './src/main.dev.tsx',
  mode: env.production ? 'production' : 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    ...(env.production
      ? {
          publicPath: `/lessons_data/${config.name}/`,
          libraryTarget: 'jsonp',
          libraryExport: 'default',
        }
      : {
          publicPath: '/',
        }),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(svg|png|jpe?g|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 4096,
            },
          },
        ],
      },
    ],
  },
  externals: env.production
    ? [
        {
          react: 'React',
          'react-is': 'ReactIs',
          'react-dom': 'ReactDOM',
          'react-router-dom': 'ReactRouterDOM',
          'react-redux': 'ReactRedux',
          redux: 'Redux',
          'redux-dynamic-modules': 'ReduxDynamicModules',
          'redux-dynamic-modules-saga': 'ReduxDynamicModulesSaga',
          'redux-saga/effects': 'ReduxSagaEffects',
          'redux-saga': 'ReduxSaga',
          'styled-components': 'styled',
        },
        require('webpack-externalize-lodash'),
      ]
    : [],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.json'],
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
  },
  plugins: [
    new DefinePlugin({
      config: JSON.stringify({
        ...require('./public/lesson.config.json'),
        username: require('os').userInfo().username,
      }),
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'public/',
          to: '.',
          globOptions: {
            ignore: ['**/index.dev.html'],
          },
        },
      ],
    }),
    ...(env.production
      ? prodPlugins
      : [
          new HtmlWebpackPlugin({
            template: 'public/index.dev.html',
          }),
        ]),
    new CleanWebpackPlugin(),
  ],
  optimization: {
    runtimeChunk: false,
    splitChunks: env.production
      ? {
          // chunks: 'all',
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendors',
              chunks: 'all',
            },
          },
        }
      : false,
  },
  devServer: {
    contentBase: [path.join(__dirname, 'dist')],
    // contentBasePublicPath: ['/'],
    compress: false,
    port: 3000,
    hot: true,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers':
        'X-Requested-With, content-type, Authorization',
    },
  },
});
