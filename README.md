# Урок

> Шаблон проекта урока для образовательной платформы [sber.study](https://sber.study)

## Содержание

- [Установка](#Установка)
- [Обновление]
- [Использование](#Использование)
- [Поддержка](#Поддержка)

## Установка

```sh
git clone https://gitlab.com/maging-team/lesson-boilerplate
cd lesson-boilerplate
yarn
```

## Обновиться до нового бойлерплейта

```sh
git remote add upstream https://gitlab.com/maging.studio/lesson-boilerplate.git
git fetch upstream
git checkout master
git rebase upstream/master
```

## Использование

Запустить с **hot reload**:

```sh
yarn start
```

Скомпилировать в дев режиме

```sh
yarn build
```

Скомпилировать в продрежиме

```sh
yarn build:prod
```

Для запуска `build` и `build:prod` урока необходимо поднять локально саму платформу и указать webpack'у из какой директории отдавать статические фалы уроков.

## Поддержка

Пишите нам в телегу или whats up
