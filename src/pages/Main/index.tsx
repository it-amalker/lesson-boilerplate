import React from 'react';
import PandaImg from './panda.jpg';

console.log({ PandaImg });

export const Main = () => (
  <div>
    <h1>Hello I am Main page of a lesson</h1>
    <img src={PandaImg} alt="" />
  </div>
);
