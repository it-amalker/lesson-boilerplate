import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  background-color: olive;
  & > button {
    margin: 1em;
    padding: 0.5em;
  }
`;

export const Sub = () => {
  const count = useSelector<{ counter: number }>(state => state.counter);
  const dispatch = useDispatch();
  return (
    <StyledWrapper>
      <h1>Hello I am Sub page of a lesson </h1>
      <h1>{count}</h1>
      <div>
        <button onClick={() => dispatch({ type: 'DECREMENT' })}>-</button>
        <button onClick={() => dispatch({ type: 'INCREMENT' })}>+</button>
      </div>
    </StyledWrapper>
  );
};
