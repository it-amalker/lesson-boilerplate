import React from 'react';
import ReactDOM from 'react-dom';
import { LessonComponent } from './main';
import { Provider } from 'react-redux';
import { createStore } from 'redux-dynamic-modules';
import { getSagaExtension } from 'redux-dynamic-modules-saga';
import { Switch, Route, Redirect, Router } from 'react-router-dom';
import { getLessonModule } from './module';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const store = createStore(
  {
    initialState: {
      /** initial state */
    },
    enhancers: [
      /** enhancers to include */
    ],
    extensions: [
      /** extensions to include i.e. getSagaExtension(), getThunkExtension() */
      getSagaExtension(),
    ],
  },
  getLessonModule()
  /* ...any additional modules */
);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <LessonComponent />
    </Router>
  </Provider>,
  document.getElementById('lesson-root')
);
