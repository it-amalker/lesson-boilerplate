import React, { useEffect } from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import { DynamicModuleLoader } from 'redux-dynamic-modules';

import { Main } from './pages/Main';
import { Sub } from './pages/Sub';
import { getLessonModule } from './module';
import './index.css';

export const LessonComponent: React.FC = () => {
  React.useEffect(() => {
    console.log('INSIDE MAIN_TEST/useEffect');
  }, []);

  // const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch({ type: 'SHOW_HEADER' });
  // }, []);
  console.log(config);

  return (
    <DynamicModuleLoader modules={[getLessonModule()]}>
      <div style={{ marginTop: 120 }} id={'lesson-root'}>
        <h1>
          Hello{' '}
          {
            // @ts-ignore
            config.username.charAt(0).toUpperCase() + config.username.slice(1)
          }
          , я из другой репы
        </h1>
        <div>
          <ul>
            <li>
              <Link to={`/lesson/${config.name}/main`}>main</Link>
            </li>
            <li>
              <Link to={`/lesson/${config.name}/sub`}>sub</Link>
            </li>
          </ul>
        </div>
        <Switch>
          <Route path={`/lesson/${config.name}/main`} component={Main} />
          <Route path={`/lesson/${config.name}/sub`} component={Sub} />
        </Switch>
      </div>
    </DynamicModuleLoader>
  );
};

const LessonWrapper = async () => {
  // console.log({ __webpack_public_path__: __webpack_public_path__ });

  // __webpack_public_path__ = `http://localhost:5000/lesson/${config.name}/`;
  // console.log({ __webpack_public_path__: __webpack_public_path__ });

  // const Main = (await import('./pages/Main')).Main;
  // const Sub = (await import('./pages/Sub')).Sub;
  // const getLessonModule = (await import('./module')).getLessonModule;
  return LessonComponent;
};
export default LessonWrapper;
