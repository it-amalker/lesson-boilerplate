import { ISagaModule } from 'redux-dynamic-modules-saga';

export function getLessonModule(): ISagaModule<{ counter: number }> {
  return {
    id: 'lesson',
    reducerMap: {
      counter: (state = 0, action) => {
        switch (action.type) {
          case 'INCREMENT':
            return state + 1;
          case 'DECREMENT':
            return state - 1;
          default:
            return state;
        }
      },
    },
    // Actions to fire when this module is added/removed
    // initialActions: [],
    // finalActions: []
  };
}
